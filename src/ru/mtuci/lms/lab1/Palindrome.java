package ru.mtuci.lms.lab1;
//Класс для определения, является ли слово палиндромом
public class Palindrome {
    //Метод для проверки, является ли каждый из переданных аргументов палиндромом
    public static void main (String[] args){
        for (int i = 0;i < args.length; i++){

            String s = args[i];
            String text = isPalindrome(s) ? "Слово " + s + " является палиндромом" :
                    "Слово " + s + " не является палиндромом";
            System.out.println(text);
        }
    }
    //Метод для переворачивания строки
    public static String reverseString(String Strings){
            String reversedString = "";
        for (int i = Strings.length() - 1; i >= 0; i--){
            reversedString += Strings.charAt(i);

        }

        return reversedString;
    }
    // Метод для определения, является ли строка палиндромом
    public static boolean isPalindrome(String s){

       return s.equals(reverseString(s));
    }
}
