package ru.mtuci.lms.lab1;

//Класс, который помогает определить, является ли число простым.
public class Primes {
    // Метод для генерации данных и вывода на экран тоько простых чисел
    public static void main(String[] args){

        for (int i = 2; i <= 100; i++){
            if (isPrime(i)) {
                System.out.println(i);
            }
        }

    }
    //Метод для определения того, является ли число простым.
    public static boolean isPrime(int n){
        if (n <= 1){
            return false;
        }

        for(int i = 2; i < n; i++){

            if (n % i == 0){
                return false;
            }
        }
        return true;


    }
}