package ru.mtuci.lms.lab5;

import java.awt.geom.Rectangle2D;

/**
 * Класс помогающий в Вычислении фрактала Трикорн
 */
public class Tricorn extends FractalGenerator {

    public static final int MAX_ITERATIONS = 2000;

    /**
     * Метод позволяет генератору
     * фракталов определить наиболее «интересную» область комплексной плоскости
     * для конкретного фрактала
     */
    @Override
    public void getInitialRange(Rectangle2D.Double range) {
        range.x = -2;
        range.y = -2;
        range.width = 4;
        range.height = 4;
    }

    /**
     * Метод реализует итеративную
     * функцию для фрактала Трикорн
     * с ограничением максимальной итерации равной 2000
     */
    @Override
    public int numIterations(double x, double y) {
        int counter = 0;
        double simpleValue = 0;
        double complexValue = 0;
        while (counter < MAX_ITERATIONS && simpleValue * simpleValue + complexValue * complexValue < 4) {

            double tempSimple = (simpleValue*simpleValue - complexValue*complexValue) + x;
            double tempComplex = (-2 * simpleValue * complexValue) + y;
            simpleValue = tempSimple;
            complexValue = tempComplex;
            counter++;
        }
        if (counter == MAX_ITERATIONS ) {
            return -1;
        }
        return counter;
    }

    /**
     * Метод возвращающий название построения фракталов.
     */
    public String toString(){
        return "Tricorn";
    }
}
