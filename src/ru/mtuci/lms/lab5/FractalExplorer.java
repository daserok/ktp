package ru.mtuci.lms.lab5;


import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.Objects;

/**
 * класс FractalExplorer, который позволяет исследовать
 * различные области фрактала, путем его создания, отображения через
 * графический интерфейс Swing и обработки событий, вызванных
 * взаимодействием приложения с пользователем.
 *
 */
public class FractalExplorer {

    private int fullResolution;
    private JImageDisplay image;
    private FractalGenerator generator;
    private Rectangle2D.Double recDouble;
    private JComboBox<String> comboBox;
    /**
        Инициализатор класса.
     */
    public FractalExplorer(int size){
        fullResolution = size;
        generator = new Mandelbrot();
        recDouble = new Rectangle2D.Double();
        generator.getInitialRange(recDouble);


    }
    /**
     Метод для создания и отображения GUI составляющей. Создаёт jFrame и кнопку очистки фрейма.
     */
    public void createAndShowGUI (){
        // Создание элементов
        JFrame jFrame = new JFrame("Fractal Explorer");
        image = new JImageDisplay(this.fullResolution, this.fullResolution);
        JButton clearButton = new JButton("Сбросить отображение");
        JButton saveButton = new JButton("Сохранить изображение");
        JPanel fractalPanel = new JPanel();
        JPanel buttonPanel = new JPanel();
        JLabel fractalText = new JLabel("Фрактал:");
        comboBox = new JComboBox<String>();

        // Установка экшн команды для кнопок
        clearButton.setActionCommand("clear");
        saveButton.setActionCommand("save");

        //Сбор comboBox
        FractalGenerator mandelbrot = new Mandelbrot();
        comboBox.addItem(mandelbrot.toString());
        FractalGenerator tricorn = new Tricorn();
        comboBox.addItem(tricorn.toString());
        FractalGenerator burningShip = new BurningShip();
        comboBox.addItem(burningShip.toString());

        // Сбор fractalPanel
        fractalPanel.add(fractalText);
        fractalPanel.add(comboBox);

        // Сбор нижней панели кнопок
        buttonPanel.add(saveButton);
        buttonPanel.add(clearButton);

       // Отрисовка jFrame по указанию расположения
        jFrame.add(image, BorderLayout.CENTER);
        jFrame.add(buttonPanel, BorderLayout.SOUTH);
        jFrame.add(fractalPanel, BorderLayout.NORTH);

        // Handler перехвата экшенов и  нажатия на мышь
        FractalHandler fractalHandler = new FractalHandler();
        comboBox.addActionListener(fractalHandler);
        clearButton.addActionListener(fractalHandler);
        saveButton.addActionListener(fractalHandler);
        jFrame.addMouseListener(new MouseHandler());



        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

       // Настройка главного окна
        jFrame.pack();
        jFrame.setVisible(true);
        jFrame.setResizable(false);

    }
    /**
     Метод вывода на экран фрактала.
     Этот метод циклически проходит через каждый пиксель в отображении
     И вычисляет кол-во итераций  для координат
     Если число итераций = -1, то пиксель закрашивается в черный цвет
     Иначе значение цвета основывается на количестве итераций
     */
    private void drawFractal (){

        double xCoord, yCoord = 0;


        for (int x = 0; x < fullResolution; x++){
            xCoord = generator.getCoord(recDouble.x, recDouble.x + recDouble.width,
                    fullResolution, x);
            for (int y = 0; y < fullResolution; y++) {

                yCoord = generator.getCoord(recDouble.y, recDouble.y + recDouble.height,
                        fullResolution, y);
               int  NumOfCounter = generator.numIterations(xCoord, yCoord);

                if (NumOfCounter == -1) {
                    image.drawPixel(x, y, 0);

                }

                else {
                    float hue = 0.7f + (float) NumOfCounter / 200f;
                    int rgbColor = Color.HSBtoRGB(hue, 1f, 1f);
                    image.drawPixel(x,y, rgbColor);
                }
            }
        }
        image.repaint();
    }


    /**
     * При получении события о щелчке мышью, класс
     * отображает пиксельные кооринаты щелчка в область фрактала, а затем вызывает
     * метод генератора recenterAndZoomRange() с координатами, по которым
     * произошел клик, и масштабом 0.5. Таким образом, нажимая на какое-либо место на
     * фрактальном отображении, изображение увеличивается.
     */
    private class MouseHandler extends MouseAdapter
    {
        public void mouseClicked(MouseEvent e)
        {
            double xCoord = FractalGenerator.getCoord(recDouble.x, recDouble.x + recDouble.width,
                    fullResolution, e.getX());
            double yCoord = FractalGenerator.getCoord(recDouble.y, recDouble.y + recDouble.height,
                    fullResolution, e.getY());

            generator.recenterAndZoomRange(recDouble,xCoord, yCoord, 0.5);

            drawFractal();
        }
    }

    /**
     * внутренний класс для обработки событий
     * от кнопок сброса, сохранения и обработчик событий при смене элемента в comboBox.
     * Обработчик сбрасывает
     * диапазон к начальному, определенному генератором,
     * а затем перерисовывает фрактал.
     */
    private class FractalHandler implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String cmd = e.getActionCommand();
            // Если экшн получен от comboBox, то проинициализировать необходиый класс.
           if (e.getSource() == comboBox){
               String item = Objects.requireNonNull(comboBox.getSelectedItem()).toString();
               switch (item){
                   case "Mandelbrot":
                       generator = new Mandelbrot();
                       break;
                   case "Tricorn":
                       generator = new Tricorn();
                       break;
                   case "Burning Ship":
                       generator = new BurningShip();
                       break;
                   default: break;
               }
               recDouble = new Rectangle2D.Double();
               generator.getInitialRange(recDouble);
               drawFractal();

           }
           //Функционал после нажатия кнопки очистки изображения.
           if (cmd.equals("clear")){
               recDouble = new Rectangle2D.Double();
               generator.getInitialRange(recDouble);
               drawFractal();

           }
           //Функционал после нажатия на кнопку "сохранить".
           else if(cmd.equals("save")){
               JFileChooser showDialog = new JFileChooser();
               FileNameExtensionFilter pngFilter = new FileNameExtensionFilter("PNG", "png");
               showDialog.setFileFilter(pngFilter);
               showDialog.setAcceptAllFileFilterUsed(false);
                // Нажал ли пользователь кнопку "Сохранить".
               if (showDialog.showSaveDialog(image) == JFileChooser.APPROVE_OPTION){
                   java.io.File file = showDialog.getSelectedFile();
                   String fileName = file.toString();
                   String filePath = file.getPath();
                   if(!filePath.toLowerCase().endsWith(".png"))
                   {
                       file = new java.io.File(filePath + ".png");
                   }


                   try {
                      BufferedImage bufferedImage = image.getImage();
                       javax.imageio.ImageIO.write(bufferedImage, "png", file);
                   }

                   catch (Exception ex){
                       JOptionPane.showMessageDialog(image, "Произошла ошибка при сохранении файла " + fileName
                       + " Лог ошибки:" + ex.getMessage());

                   }

               }

            }
        }
    }

    /**
     * Инициализирование нового экземпляра класса FractalExplorer с
     * размером отображения 800
     * Вызов метода на отображение GUI
     * И отрисовка фрактала
     */
    public static void main(String[] args) {
        FractalExplorer fractalExplorer = new FractalExplorer(600);
        fractalExplorer.createAndShowGUI();
        fractalExplorer.drawFractal();

    }

}
