package ru.mtuci.lms.lab5;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Класс позволяет нам отрисовать наши фракталы.
 */
public class JImageDisplay extends JComponent {
    private BufferedImage image;

    /**
     *Конструктор, который принимает целочисленные
     * значения ширины и высоты, и инициализирует объект BufferedImage новым
     * изображением с этой шириной и высотой, и типом изображения
     * TYPE_INT_RGB
     */
    public  JImageDisplay(int weight, int height) {
        image = new BufferedImage(weight, height,BufferedImage.TYPE_INT_RGB);
        Dimension dim = new Dimension(weight, height);
        super.setPreferredSize(dim);
    }

    public BufferedImage getImage() {
        return image;
    }
    /**
     *Метод для отрисовки, переопределющий защищенный метод JComponent
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage (image, 0, 0, image.getWidth(), image.getHeight(), null);
    }

    /**
     * Метод устанавливает все пиксели изображения в черный цвет (значение RGB 0)
     */
    public void clearImage(){
        image.setRGB(0, 0 , image.getWidth(), image.getHeight(), null, 0,1);
    }

    /**
     * Метод, который устанавливает пиксель в определенный цвет.
     */
    public void  drawPixel (int x, int y, int rgbColor){
        image.setRGB(x, y, rgbColor);
    }

}
