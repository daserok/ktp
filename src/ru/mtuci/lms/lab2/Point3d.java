package ru.mtuci.lms.lab2;
/**
 * трёхмерный класс точки.
 **/
public class Point3d {
    /** координата X **/
    private double xCoord;
    /** координата Y **/
    private double yCoord;
    /** координата Z **/
    private double zCoord;
    /** Конструктор инициализации **/
    public Point3d ( double x, double y, double z) {
        xCoord = x;
        yCoord = y;
        zCoord = z;
    }

    /** Конструктор по умолчанию. **/
    public Point3d () {
//Вызовите конструктор с двумя параметрами и определите источник.
        this(0.0, 0.0,0.0);
    }

    /** Возвращение координаты X **/
    public double getX () {
        return xCoord;
    }
    /** Возвращение координаты Y **/
    public double getY () {
        return yCoord;
    }

    /** Возвращение координаты Z **/
    public double getZ () {
        return zCoord;
    }

    /** Установка значения координаты X. **/
    public void setX ( double val) {
        xCoord = val;
    }
    /** Установка значения координаты Y. **/
    public void setY ( double val) {
        yCoord = val;
    }

    /** Установка значения координаты Z. **/
    public void setZ ( double val) {
        zCoord = val;
    }
    /** Метод сравнения двух точек на равенство **/
    public boolean isPointsEquals (Point3d Object3D){
        return (this.xCoord == Object3D.xCoord &&
                this.yCoord == Object3D.yCoord &&
                this.zCoord == Object3D.zCoord);
    }
    /** Метод для подсчета расстояния между двумя точками **/
    public double distanceTo(Point3d Object3D){
        return Math.sqrt(Math.pow((Math.round(Object3D.xCoord * 100) / 100D - Math.round(this.xCoord * 100) / 100D),2) +
                Math.pow((Math.round(Object3D.yCoord * 100) / 100D - Math.round(this.yCoord * 100) / 100D),2) +
                Math.pow((Math.round(Object3D.zCoord * 100) / 100D - Math.round(this.zCoord * 100) / 100D),2));
    }


}

