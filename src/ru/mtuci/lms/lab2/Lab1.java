package ru.mtuci.lms.lab2;

import java.util.ArrayList;
import java.util.Scanner;

/** Класс для ввода данных пользователя и подсчета площади треугольника по 3-м точкам**/
public class Lab1 {

    public static void main(String[] args) {
        double x, y, z;
        ArrayList <Point3d> points = new ArrayList<>();
        // Считывание данных от пользователя
        Scanner inData = new Scanner(System.in);
        for (int i=1; i<4; i++){
        System.out.println("введите координаты" + " точки " + i);
        System.out.println("Введите координату X");
        x = inData.nextDouble();
        System.out.println("Введите координату Y");
        y = inData.nextDouble();
        System.out.println("Введите координату Z");
        z = inData.nextDouble();
        points.add(new Point3d(x,y,z));
        }
        // Проверка на уникальность точек
        if (points.get(0).isPointsEquals(points.get(1)) ||
            points.get(1).isPointsEquals(points.get(2)) ||
            points.get(0).isPointsEquals(points.get(2))){
         throw new java.lang.RuntimeException("\"Координаты имеют одинаковые точки. Выполнение работы программы невозможно\"");
        }
        // Расчёт площади по заданнным точкам
        double area =  computeArea(points.get(0), points.get(1), points.get(2));
        System.out.println("Площадь треугольника равна " + area);
    }
    /** Метод для подсчета площади треугольника **/
    public static double computeArea(Point3d obj1, Point3d obj2, Point3d obj3){
        double a = obj1.distanceTo(obj2);
        double b = obj2.distanceTo(obj3);
        double c = obj1.distanceTo(obj3);

        double p = 0.5 *  (a + b + c);

        return Math.sqrt(p * (p-a) * (p-b) * (p-c));

    }
}
