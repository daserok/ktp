package ru.mtuci.lms.lab3;

/**
 * This class represents a specific location in a 2D map.  Coordinates are
 * integer values.
 **/
public class Location
{
    /** X coordinate of this location. **/
    public int xCoord;

    /** Y coordinate of this location. **/
    public int yCoord;


    /** Creates a new location with the specified integer coordinates. **/
    public Location(int x, int y)
    {
        xCoord = x;
        yCoord = y;
    }

    /** Creates a new location with coordinates (0, 0). **/
    public Location()
    {
        this(0, 0);
    }

    @Override
    public int hashCode() {
        final int prime = 32;
        int result = 1;
        result += 10 * prime + xCoord;
        result += 20 * prime + yCoord;

        return result;
    }
     @Override
    public boolean equals (Object object) {
        if (object == this){
            return true;
        }
        if (object instanceof Location)
        {
            Location newLocation = (Location) object;
            return this.xCoord == newLocation.xCoord && this.yCoord == newLocation.yCoord;
        }

        return false;
    }

}