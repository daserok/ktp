package ru.mtuci.lms.lab4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;

/**
 * класс FractalExplorer, который позволяет исследовать
 * различные области фрактала, путем его создания, отображения через
 * графический интерфейс Swing и обработки событий, вызванных
 * взаимодействием приложения с пользователем.
 *
 */
public class FractalExplorer {

    private int fullResolution;
    private JImageDisplay image;
    private  FractalGenerator generator;
    private Rectangle2D.Double recDouble;
    /**
        Инициализатор класса.
     */
    public FractalExplorer(int size){
        fullResolution = size;
        generator = new Mandelbrot();
        recDouble = new Rectangle2D.Double();
        generator.getInitialRange(recDouble);


    }
    /**
     Метод для создания и отображения GUI составляющей. Создаёт jFrame и кнопку очистки фрейма.
     */
    public void createAndShowGUI (){
        JFrame jFrame = new JFrame("Fractal Explorer");
        image = new JImageDisplay(this.fullResolution, this.fullResolution);
        JButton clearButton = new JButton("Сбросить отображение");

        jFrame.add(image, BorderLayout.CENTER);
        jFrame.add(clearButton, BorderLayout.SOUTH);
        clearButton.addActionListener(new ResetHandler());

        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.addMouseListener(new MouseHandler());

        jFrame.pack();
        jFrame.setVisible(true);
        jFrame.setResizable(false);

    }
    /**
     Метод вывода на экран фрактала.
     Этот метод циклически проходит через каждый пиксель в отображении
     И вычисляет кол-во итераций  для координат
     Если число итераций = -1, то пиксель закрашивается в черный цвет
     Иначе значение цвета основывается на количестве итераций
     */
    private void drawFractal (){

        double xCoord, yCoord = 0;


        for (int x = 0; x < fullResolution; x++){
            xCoord = generator.getCoord(recDouble.x, recDouble.x + recDouble.width,
                    fullResolution, x);
            for (int y = 0; y < fullResolution; y++) {

                yCoord = generator.getCoord(recDouble.y, recDouble.y + recDouble.height,
                        fullResolution, y);
               int  NumOfCounter = generator.numIterations(xCoord, yCoord);

                if (NumOfCounter == -1) {
                    image.drawPixel(x, y, 0);

                }

                else {
                    float hue = 0.7f + (float) NumOfCounter / 200f;
                    int rgbColor = Color.HSBtoRGB(hue, 1f, 1f);
                    image.drawPixel(x,y, rgbColor);
                }
            }
        }
        image.repaint();
    }

    /**
     * внутренний класс для обработки событий
     * от кнопки сброса. Обработчик сбрасывает
     * диапазон к начальному, определенному генератором,
     * а затем перерисовывает фрактал.
     */
    private class ResetHandler implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            recDouble = new Rectangle2D.Double();
            generator.getInitialRange(recDouble);

            drawFractal();
        }
    }

    /**
     * При получении события о щелчке мышью, класс
     * отображает пиксельные кооринаты щелчка в область фрактала, а затем вызывает
     * метод генератора recenterAndZoomRange() с координатами, по которым
     * произошел клик, и масштабом 0.5. Таким образом, нажимая на какое-либо место на
     * фрактальном отображении, изображение увеличивается.
     */
    private class MouseHandler extends MouseAdapter
    {
        public void mouseClicked(MouseEvent e)
        {
            double xCoord = FractalGenerator.getCoord(recDouble.x, recDouble.x + recDouble.width,
                    fullResolution, e.getX());
            double yCoord = FractalGenerator.getCoord(recDouble.y, recDouble.y + recDouble.height,
                    fullResolution, e.getY());

            generator.recenterAndZoomRange(recDouble,xCoord, yCoord, 0.5);

            drawFractal();
        }
    }

    /**
     * Инициализирование нового экземпляра класса FractalExplorer с
     * размером отображения 800
     * Вызов метода на отображение GUI
     * И отрисовка фрактала
     */
    public static void main(String[] args) {
        FractalExplorer fractalExplorer = new FractalExplorer(800);
        fractalExplorer.createAndShowGUI();
        fractalExplorer.drawFractal();

    }

}
